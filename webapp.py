# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 15:00:38 2022

@author: eljar
"""

import streamlit as st
import pandas as pd
import joblib
import numpy as np

rad = st.sidebar.radio("Navigering", ["Søk om lån", "Om oss", "Data"])

if rad == "Søk om lån":

    st.title("Automatisk Lån Evaluering")
    
    st.header("Velkommen!")
    
    st.subheader("Vennligst angi din informasjon nedenfor for å se om du er kvalifisert for lån.")
    
    Kjønn = st.selectbox("Kjønn", ("Mann", "Dame"))
    
    Gift = st.selectbox("Er du gift?", ("Ja", "Nei"))
    
    Avhengige = st.number_input("Antall barn under 18 og/eller ektefelle uten arbeid.", max_value = 5, step=1)
    desc1 = '<p style="font-family:sans-serif; color:Grey; font-size: 10px;">Maks 5 for automatisert  søknadsevaluering av søknaden</p>'
    st.markdown(desc1, unsafe_allow_html=True)
    Utdanning = st.selectbox("Har du utdanning?", ("Ja", "Nei"))
    
    Selvstendignæringsgivende = st.selectbox("Er du selvstendig næringsgivende?", ("Ja", "Nei"))
    
    Søkersinntekt = st.number_input("Søkers inntekt.", step=1)
    
    Cosøkersinntekt = st.number_input("Co-Søkers inntekt.", step=1)
    
    Lånemengde = st.number_input("Lånemengde i tusen.", max_value = 600, step=1)
    desc1 = '<p style="font-family:sans-serif; color:Grey; font-size: 10px;">Maks 600 for automatisert  søknadsevaluering av søknaden</p>'
    st.markdown(desc1, unsafe_allow_html=True)
    
    Lånelengde = st.number_input("Lengde på lån i måneder.", step=1)
    
    Kreditthistorie = st.selectbox("Møter kreditten rettningslinjene?", ("Ja", "Nei"))
    
    Bo = st.selectbox("Ditt boområde?", ("På Landet", "Semi-by", "By"))
    
    if st.button("Bekreft Opplysninger."):
        
        clas = joblib.load("Classifier.pkl")
        
        X = pd.DataFrame([[Kjønn, Gift, Avhengige, Utdanning, Selvstendignæringsgivende, Søkersinntekt, 
                           Cosøkersinntekt, Lånemengde, Lånelengde, Kreditthistorie, Bo]], 
                         columns = ["Gender", "Married", "Dependents", "Education", "Self_Employed",
                                    "ApplicantIncome", "CoapplicantIncome", "LoanAmount", "Loan_Amount_Term", "Credit_History", "Property"])
        X = X.replace(["Ja", "Nei", "Mann", "Dame", "På Landet", "Semi-by", "By"], [1, 0, 1, 0, 0, 1, 2])
        prediction = clas.predict(X)[0]
        if prediction == 1:
            st.text("LÅNESØKNADEN DIN ER BLITT GODKJENT!")
        else:
            st.text("LÅNESØKNADEN DIN ER DESSVERRE BLITT AVSLÅTT")
        st.text(f"{prediction}")

if rad == "Om oss":
    st.title("Litt om oss")
    
    st.header("Automatisert lån evaluering")
    
    st.markdown("Bakgrunnen bak denne modellen er å presentere en alternativ løsning til dagens manuelle behanlding av lånesøknader, en automatisert løsning ved hjelp av maskinlæring")
    
    st.markdown("Modellen er en del av en designrapport, trykk linken under hvis du er interessert i å vite mer om modellen")
    
if rad == "Data":
    st.title("Data")
    
    st.header("Dataen brukt for modellen")
    
    data = pd.read_csv(r"C:\Users\eljar\Documents\TIN200\Låndata\train.csv")
    
    data
    
    