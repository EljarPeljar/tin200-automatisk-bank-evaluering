# -*- coding: utf-8 -*-
"""
Created on Fri Jun  3 12:00:36 2022

@author: eljar
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
import joblib as jbl

test_r = pd.read_csv(r"C:\Users\eljar\Documents\TIN200\Låndata\test.csv")
train_r = pd.read_csv(r"C:\Users\eljar\Documents\TIN200\Låndata\train.csv")

test_r = test_r.drop("Loan_ID", 1)
train_r = train_r.drop("Loan_ID", 1)

#PREPROSSESERING
test_r['Married'] = test_r['Married'].replace(['Yes','No'],[1, 0])
train_r['Married'] = train_r['Married'].replace(['Yes','No'],[1, 0])

test_r['Gender'] = test_r['Gender'].replace(['Male','Female'],[1, 0])
train_r['Gender'] = train_r['Gender'].replace(['Male','Female'],[1, 0])

test_r['Education'] = test_r['Education'].replace(['Graduate','Not Graduate'],[1, 0])
train_r['Education'] = train_r['Education'].replace(['Graduate','Not Graduate'],[1, 0])

test_r['Self_Employed'] = test_r['Self_Employed'].replace(['Yes','No'],[1, 0])
train_r['Self_Employed'] = train_r['Self_Employed'].replace(['Yes','No'],[1, 0])

train_r['Loan_Status'] = train_r['Loan_Status'].replace(['Y','N'],[1, 0])

test_r['Dependents'] = test_r['Dependents'].replace(["0", '1','2', "3+"],[0, 1, 2, 3])
train_r['Dependents'] = train_r['Dependents'].replace(["0", '1','2', "3+"],[0, 1, 2, 3])

test_r['Property_Area'] = test_r['Property_Area'].replace(['Rural','Semiurban', "Urban"],[0, 1, 2])
train_r['Property_Area'] = train_r['Property_Area'].replace(['Rural','Semiurban', "Urban"],[0, 1, 2])

#NAN VALUES
NanValues = train_r.isnull().sum()
NanValues1 = test_r.isnull().sum()

train_r = train_r.dropna()
test_r = test_r.dropna()
#VISUALIZATION
fig,ax = plt.subplots(2,4,figsize=(16,10))
sns.countplot("Gender",data=train_r,ax=ax[0][0])
sns.countplot("Married",data=train_r,ax=ax[0][1])
sns.countplot("Dependents",data=train_r,ax=ax[0][2])
sns.countplot("Education",data=train_r,ax=ax[0][3])
sns.countplot("Self_Employed",data=train_r,ax=ax[1][0])
sns.countplot("Credit_History",data=train_r,ax=ax[1][1])
sns.countplot("Property_Area",data=train_r,ax=ax[1][2])
sns.countplot("Loan_Status",data=train_r,ax=ax[1][3])
#CORRELATIOn
CORR = train_r.corr()

#SPLITTING TRAIN TEST
train_y = train_r["Loan_Status"]
train_r = train_r.drop("Loan_Status", 1)
[x_train, x_test, y_train, y_test] = \
    train_test_split(train_r, train_y,
                     test_size = 0.2,
                     random_state=1)


x_test_w = x_test.copy()
x_test_m = x_test.copy()

x_test_m["Gender"] = x_test_m["Gender"].replace([0], [1])
x_test_w["Gender"] = x_test_w["Gender"].replace([1], [0])

test_r_w = test_r.copy()
test_r_m = test_r.copy()

test_r_m["Gender"] = test_r_m["Gender"].replace([0], [1])
test_r_w["Gender"] = test_r_w["Gender"].replace([1], [0])

Pipeline0 = make_pipeline(StandardScaler(),
       #                   RandomForestClassifier())
                          SVC(C=1, max_iter=100))

param_grid0 = [
            #   {"randomforestclassifier__n_estimators": [10, 100, 1000]} 
               {"svc__C": [0.0001, 0.001, 0.01, 1, 1.1 , 100, 1000]},
               {"svc__max_iter": [90, 100, 120, 400]}
]

S = GridSearchCV(estimator = Pipeline0,
                       param_grid = param_grid0,
                       cv = 6,
                       scoring="accuracy")

Pipeline0.fit(x_train, y_train)

S.fit(x_train, y_train)

best_mod1 = S.best_estimator_

best_mod1.fit(x_train, y_train)

y_pred2 = best_mod1.predict(x_test)

print('Test Accuracy2: %.3f' % best_mod1.score(x_test, y_test))


y_pred = best_mod1.predict(test_r)
count = (y_pred==0).sum()
y_pred_w = best_mod1.predict(test_r_w)
count_w = (y_pred_w==0).sum()
y_pred_m = best_mod1.predict(test_r_m)
count_m = (y_pred_m==0).sum()

jbl.dump(Pipeline0, "Classifier.pkl")

#print("confusion matrix\n", confusion_matrix(y_test, y_pred))

